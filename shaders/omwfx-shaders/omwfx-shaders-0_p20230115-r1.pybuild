# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.git import Git
from common.mw import MW, InstallDir


class Package(Git, MW):
    NAME = "OMWFX Shaders"
    DESC = "A set of post processing shaders"
    HOMEPAGE = "https://gitlab.com/vtastek/omwfx-shaders"
    GIT_SRC_URI = "https://gitlab.com/vtastek/omwfx-shaders.git"
    GIT_COMMIT_DATE = "2023-01-15"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    KEYWORDS = "-openmw{<0.48}"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="omwfx-shaders",
            # Causes an 80 FPS drop on my system
            # PLUGINS=[File("Cinematic Boken DoF.omwscripts")],
            DOC=["README.md"],
        )
    ]

    def pkg_pretend(self):
        super().pkg_pretend()

        self.warn(
            "This mod only works with OpenMW 0.48 (currently a development build)"
        )

        self.info(
            """
To enable the shaders, press F2, select the shader you want, and click
on the right arrow.

More information: https://openmw.readthedocs.io/en/latest/reference/postprocessing/overview.html
            """
        )
