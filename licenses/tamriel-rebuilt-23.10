Please credit the original authors first if the asset you want to use appears in this list (search using the corresponding Construction Set ID without suffix numbers); "edited" indicates that the meshes, textures, or text of the original asset have been altered or compressed in TR.

The following asset list aims to include:
	- By default, all separately released resources that were not directly contributed to TR by their authors but included based on their usage permissions, starting with the "Sacred East" release (June 2012).
	- Additionally, any assets from contributors past, present, or future who request separate documentation here.

This is retroactively documented before 2017 and therefore prone to oversights. Should anyone find they have been omitted from this list, please let us know.

Author:    Momo77
---------------------------------
Added Feb 2012, from: Momos Crane Resource - http://www.nexusmods.com/morrowind/mods/41074/?
IDs:    T_Com_SetHarbor_CraneLarge_01 to 03
    	T_Com_SetHarbor_CraneMiddle_01 to 02
    	T_Com_SetHarbor_CranePlatfL_01
    	T_Com_SetHarbor_CranePlatfS_01
    	T_Com_SetHarbor_CranePlatfVL_01
    	T_Com_SetHarbor_CraneSmall_01
Added Feb 2012, from: Momos Fishery Resource - http://www.nexusmods.com/morrowind/mods/41173/?
IDs:    T_Com_SetHarbor_DipNet_01 to 02
    	T_Com_SetHarbor_FishBeheaded_01 to 02
    	T_Com_SetHarbor_FishHanging_01 to 02
    	T_Com_SetHarbor_Fishnet_01 to 02
    	T_Com_SetHarbor_FishTrap_01
    	T_Com_SetHarbor_Rope_01

Author:    Slartibartfast
---------------------------------
Added Aug 2012, from: Smith Shed Resource - http://www.nexusmods.com/morrowind/mods/42183/?
IDs:    T_Imp_SetMw_X_SmithShed_01
    	T_Imp_SetNord_X_SmithShed_01
    	T_Nor_SetSkaal_SmithShed_01
Added Apr 2018, from: Imperial Plaza Resource - http://mw.modhistory.com/download-31-5968
IDs:    T_Imp_LegionCyr_X_Plaza_01 to 03 (edited)
    	T_Imp_LegionMw_X_Plaza_02 to 03 (edited)
    	T_Imp_LegionSky_X_Plaza_03 to 03 (edited)
Added May 2018, from: Well Diversified - http://mw.modhistory.com/download-7-15575
IDs:    T_Imp_LegionCyr_X_Well_03 to 04 (edited)
    	T_Imp_LegionMw_X_Well_03 to 04
    	T_Imp_LegionSky_X_Well_03 to 04 (edited)
    	T_Nor_SetSkaal_X_Well_01

Author:    t'nilc
---------------------------------
Added Aug 2013, from: Hookah - http://mw.modhistory.com/download-1-11970
IDs:    T_Rga_HookahApp_01
    	T_Rga_HookahFlavouredApp_01

Author:    Midgetalien
---------------------------------
Added Aug 2013, from: Stick Fences - http://www.nexusmods.com/morrowind/mods/42471/?
IDs:    T_Com_Set_FenceBriar_01 to 03
Added Dec 2013, from: Goblin Shaman - http://mw.modhistory.com/download-44-13318
IDs:    T_Mw_Cre_GobShm_01 (edited)

Author:    dongle
---------------------------------
Added Jul 2014, from: Elizabethan Galleon - http://mw.modhistory.com/download-44-2980
IDs:    T_Imp_SetHarbor_GalleonNavy_01 (edited)
    	T_Imp_SetHarbor_GalleonInLow_01 (edited)
    	T_Imp_SetHarbor_GalleonInUp_01 (edited)

Author:    Detritus2004
---------------------------------
Added Dec 2016, from: Khajiit Faces v2.0 - http://mw.modhistory.com/download-43-3301
IDs:    T_B_Kha_HeadMaleTR_02 to 09 (edited)
    	T_B_Kha_HeadFemTR_01 to 09 (edited)
    	T_B_Kha_HairMaleTR_01 to 02

Author:    Silaria
---------------------------------
Added Dec 2016, from: Sils Argonian Heads and Hair v1.1 - http://mw.modhistory.com/download-80-3324
IDs:    T_B_Arg_HeadMaleTR_02 to 07 (edited)
    	T_B_Arg_HeadFemTR_01 to 06 (edited)
    	T_B_Arg_HairMaleTR_01 to 07 (edited)
    	T_B_Arg_HairFemTR_01 to 04 (edited)

Author:    R-Zero
---------------------------------
Added Mar 2017, from: TR Silt Strider fix - http://www.nexusmods.com/morrowind/mods/43297/?
IDs:    T_Mw_Fau_Slstrid_01

Author:    Alaisiagae
---------------------------------
Added Jul 2017, from: Imperial Silver Armor Resource - http://mw.modhistory.com/download-56-6598
IDs:    T_Imp_Silver_Boots_01 (edited)
    	T_Imp_Silver_BracerL_01 (edited)
    	T_Imp_Silver_BracerR_01 (edited)
    	T_Imp_Silver_Greaves_01 (edited)
    	T_Imp_Silver_PauldronL_01 (edited)
    	T_Imp_Silver_PauldronR_01 (edited)

Author:    YarYulme
---------------------------------
Added Jan 2018, from: Nif Resources - https://www.nexusmods.com/morrowind/mods/43064/?
IDs:    T_Imp_Uni_SwiftcutSaber

Author:    Lochnarus
---------------------------------
Added Jan 2018, from: Elite Dark Brotherhood Helm v1.3 - http://mw.modhistory.com/download-80-7683
IDs:    T_De_UNI_PreyseekerHelm (edited)

Author:    Antares
---------------------------------
Added Jan 2018, from: Undead: Arise From Death - http://mw.modhistory.com/download-55-15310
IDs:    T_Mw_Und_Mum_02 (edited)
Added Dec 2019, from: Undead: Arise From Death - http://mw.modhistory.com/download-55-15310
IDs:    T_Glb_Und_SkelArise_01
    	T_Glb_Und_SkelArise_02
    	T_Glb_Und_SkelArise_03
    	T_Glb_Und_SkelArise_04
    	T_ScCrea_SkelRise_01 (edited)
    	T_ScCrea_SkelRise_02 (edited)
    	T_ScCrea_SkelRise_03 (edited)

Author:    Lougian
---------------------------------
added Jan 2018, from: Caverns Overhaul - https://www.nexusmods.com/morrowind/mods/42249/?
IDs:    T_Glb_Light_CaveRay01 to 12 (updated)

Author:    CemKey
---------------------------------
Added Dec 2018
IDs:    T_De_SetHla_X_Striderport_01

Author:    ch_devgroup
---------------------------------
Added Dec 2018
IDs:    T_De_RedBonemold_HelmOpen_01

Author:    Ao3
---------------------------------
Added Dec 2018
IDs:    T_De_SetHla_X_EntryFake_01
    	T_De_SetHla_X_Grate_01
    	T_De_SetHla_X_Waterspout_01

Author:    PikachunoTM
---------------------------------
Added Feb 2019
IDs:    T_Nor_UNI_Targe_Blooded
    	T_Com_UNI_KingOrgnumCoffer_01
    	T_Dae_UNI_Rueful_Axe
    	T_Dae_UNI_NebCrescen
Added May 2019
IDs:    T_Glb_Fauna_Photodragons_01

Author:    Nich/ZWolol
---------------------------------
Added May 2019, from: Correct UV Rocks - https://www.nexusmods.com/morrowind/mods/46104
IDs:    T_Mw_TerrRockRR_Rock_01 to 30 (edited)

Author:    Stuporstar
---------------------------------
Added May 2019, from Graphic Herbalism - MWSE and OpenMW Edition - https://www.nexusmods.com/morrowind/mods/46599
IDs:    T_Mw_Flora_HornLily_01 to 06
        T_Mw_Flora_Nirthfly03 to 05
        T_Mw_Flora_TimsaComeB01 to 04
        T_Mw_Flora_Meadowrye01 to 04

Author:    Tyddy
---------------------------------
Added Aug 2019, from: Bigger Temples - Urbanopticum of Vvardenfell - https://www.nexusmods.com/morrowind/mods/46184
IDs:    T_De_SetVeloth_Temple_03

Author:    Lurlock/Dragon32
---------------------------------
Added: Sep 2019
IDs:    T_Mw_CaveBone_Exit_00
    	T_Mw_CaveVM_TunnelBig_05
    	T_Mw_CaveVM_RoomBig_06
    	T_Mw_CaveVM_RoomBig_07
    	T_Mw_CaveVM_RockBridge_01
    	T_Mw_CaveVM_Doorway_00
    	T_Mw_CaveVM_Exit_00
    	T_Mw_CaveVM_Exit_01
    	T_Mw_CaveVM_RockForm_00 to 03
    	T_Mw_CaveVM_TunnelSml_05
    	T_Mw_CaveVM_RoomSml2_00
    	T_Mw_CaveVM_RoomSml3_00 to 02
    	T_Mw_CaveVM_RoomSml4_01 to 05
    	T_Mw_CaveVM_RockStal_00 to 05

Author:    Danke
---------------------------------
Added Dec 2019
IDs:    T_De_TelvCephalopod_PauldL_01 (edited)
    	T_De_TelvCephalopod_PauldR_01 (edited)
    	T_De_TelvCephalopod_GauntL_01 (edited)
    	T_De_TelvCephalopod_GauntR_01 (edited)
    	T_De_TelvCephalopod_Boots_01 (edited)
    	T_De_TelvCephalopod_Greaves_01 (edited)
    	T_De_TelvCephalopod_Cuirass_01 (edited)

Author:    Rubberman
---------------------------------
Added Dec 2019
IDs:    T_MW_DngBarrow_Stairs_01

Author:    Danea123
---------------------------------
Added Dec 2019, from The Demon of Knowledge - https://www.nexusmods.com/morrowind/mods/46126
IDs:    T_Dae_UNI_OghmaInfinium

Author:    IconsPNG
---------------------------------
Added Dec 2019, from iconspng.com - https://www.iconspng.com/image/83689/magic-circle-1
IDs:    T_Dae_UNI_OghmaInfinium (edited)
		
Author:    Remiros and Melchior Dahrk
---------------------------------
Added November 2020, from OOAB Data - https://www.nexusmods.com/morrowind/mods/49042
IDs:	tr_misc_ebony_cup.nif
		tr_misc_ebony_fork.nif
		tr_misc_ebony_knife.nif
		tr_misc_ebony_l_flask.nif
		tr_misc_ebony_platter.nif
		tr_misc_ebony_spoon.nif
		tr_misc_ebony_frosted.dds
		tr_misc_ebony_gold.dds
		tr_misc_de_g_bowl_01.nif
		tr_misc_de_g_bowl_02.nif
		tr_misc_de_g_pitcher.nif
		tr_misc_de_g_plate.nif
		tr_misc_de_g_platter.nif
		tr_misc_de_g_pot.nif
		tr_app_e_alembic.nif
		tr_app_e_calcinator.nif
		tr_app_e_mortarpestle.nif
		tr_app_e_retort.nif
		tr_w_dae_throwknife.nif
		tr_w_goblin_arrow.nif
		tr_w_huntsman_arrow.nif
		tr_w_ice_arrow.nif
		tr_w_orcish_arrow.nif
		tr_w_silver_halberd.nif

Author:    YarYulme
---------------------------------
Added April 2020, from Nif Resources - https://www.nexusmods.com/morrowind/mods/43064
IDs:	tr_a_DwrvScrp_helmet.nif
		tr_a_DwrvScrp_bracer_w.nif

Author:    Quorn
---------------------------------
Added April 2020, from Quorn Resource Integration - https://www.nexusmods.com/morrowind/mods/43269
IDs:	tr_a_DwrvScrp_chest_C.nif

Author:    Heinrich
---------------------------------
Added Feb 2020, from Weapon Sheathing - https://www.nexusmods.com/morrowind/mods/46069
IDs:    tr_w_iron_dagger_02_sh.nif
		tr_w_iron_bsword_03_sh.nif
		tr_w_iron_dagger_02.nif
		tr_w_iron_bswrd_02_sh.nif
	
Author:    Lord Berandas
---------------------------------
Added Feb 2020, from Weapon Sheathing - https://www.nexusmods.com/morrowind/mods/46069
IDs:    tr_w_adamant_wakazashi_sh.nif
		tr_w_adamant_tanto_sh.nif
		tr_w_adamant_katana_sh.nif
		tr_w_adamant_dkatana_sh.nif
		tr_w_adamant_saber_sh.nif
		tr_w_adamant_lsword_sh.nif
		tr_w_adamant_dagger_sh.nif
		tr_w_adamant_bsword_sh.nif
	
Author:    InspectorJ, boaay, karlis
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_impact_door_01.wav (edited)

Author:    dirtydowntowner, djcdelahaye, 150112, schots, rutgermuller
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_impact_gate_01 (edited)

Author:    gevaroy
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_impact_glass_01 (edited)

Author:    AlanCat, SoundFlakes
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_impact_rock_01 (edited)

Author:    ToaTom
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_impact_vase_01 (edited)

Author:    Bertsz, kwahmah_02, kevinkace, ErikH2000
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_impact_wood_01 (edited)

Author:    AryaNotStark
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_brush_01 (edited)

Author:    nettimato, dylanperitz
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_dice_01 (edited)

Author:    benjaminharveydesign, ryujin95
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_hammer_01 (edited)

Author:    omnisis, kyles, qubodup, celadon, rutgermuller
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_irongate_01 (edited)
		TR_obj_irongate_02 (edited)

Author:    snardin42, 7778, Piggimon
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_shovel_01 (edited)

Author:    aurea, pauliep83, neilraouf, craigsmith, jergonda, lezaarth 
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_woodgate_01 (edited)
		TR_obj_woodgate_02 (edited)

Author:    robinhood76, zatar, jagadamba, rutgermuller
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_obj_woodslide_01 (edited)

Author:    OGsoundFX
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_sfx_ghost_01 (edited)

Author:    cell31-sound-productions, SoundFlakes
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_sfx_ghost_02 (edited)

Author:    aderumoro, slugzilla
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_sfx_ghost_03 (edited)

Author:    zatar
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_sfx_landslide_01 (edited)

Author:    jackmichaelking
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_sfx_wave_01 (edited)

Author:    SoundFlakes
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_sfx_woosh_01 (edited)

Author:    fission9, bulbastre
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_loop_abyss_01 (edited)

Author:    proxima4
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_loop_mtpeak_01 (edited)

Author:    robinhood76, klankbeeld, onderwish
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_loop_oldones_01 (edited)

Author:    kyles
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_crowd_applaud_01 (edited)

Author:    YleArkisto, unchaz, robinhood76
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_crowd_boos_01 (edited)

Author:    YleArkisto, joedeshon, robinhood76
---------------------------------
Added Sep 2020, from freesound.org
IDs:    TR_crowd_cheer_01 (edited)

Author: Enderal Team and Beyond Skyrim Assets
---------------------------------
permission granted by Larrian
Added Dec 2020, from Enderal https://www.nexusmods.com/skyrim/mods/77868/
IDs: T_Glb_Flora_Mangrove_01 to 04

Author:    ashtaar
---------------------------------
Added Dec 2020, from https://www.nexusmods.com/morrowind/mods/44398
IDs:    T_Dae_DngRuin_StatueMephala_01

Author:    TeamCutiKagouti
---------------------------------
Added Jan 2021, from https://www.nexusmods.com/morrowind/mods/44650
IDs: T_Com_Iron_Rapier_01
	T_Com_Steel_Rapier_01
	T_Imp_Legion_Dagger_02
	T_Com_Iron_Dagger_03
	T_Com_Steel_Dagger_02

Author:    Zobator
---------------------------------
Added Feb 2021, from https://www.nexusmods.com/morrowind/mods/45489/
Key replacement models

Author:    Ruffin Vangarr
---------------------------------
Added May 2021, from https://www.nexusmods.com/morrowind/mods/49534
Daedric helmets

https://www.freesoundeffects.com
---------------------------------
Vermai Sound effects (stock sound Jaguar)

https://sketchfab.com/3d-models/spyglass-b27ebbf423ae431f9dac6ab2c8790515
---------------------------------
"Spyglass" (https://skfb.ly/onnWu) by Miguel Salgueiro is licensed under Creative Commons Attribution-NonCommercial (http://creativecommons.org/licenses/by-nc/4.0/).

Author:    Echo
---------------------------------
Added Aug 2021, from Imperial Library 20th Anniversary Contest - https://www.imperial-library.info/content/dravins-rules
IDs:    TR_m4_Bal_LairVaultBook (Dravin's Rules, edited)

Author: Miguel Salgueiro
---------------------------------
From: https://skfb.ly/onnWu, licensed under Creative Commons Attribution-NonCommercial (http://creativecommons.org/licenses/by-nc/4.0/)
IDs:   T_Com_Spyglass01 (edited)

Author:    Reizeron
---------------------------------
Added Sept 2021, from:     Brevur of Balmora - Finally Some Good Statue Mod https://www.nexusmods.com/morrowind/mods/47557
IDs:    T_De_SetHla_F_Plaque
	T_De_Set_StatueBevur
	
Author:    Spok
---------------------------------
Added June 2022, via OAAB_Data
IDs:	T_Imp_LegionMw_X_Window_01
	T_Imp_LegionMw_X_Window_02

Author:    Chainy
---------------------------------
Added via OAAB_Data
scorch mark decals

Author:    Kiteflyer61 and Archipel de Pertevue
---------------------------------
Added via OAAB_Data
archery targets

Author: RubberMan
---------------------------------
Added June 2022, via OAAB_data
IDs:	T_Imp_FurnM_WeaponRack_01-02

Author: AFFA
---------------------------------
Added via AFFresh
	Cunning, Contempt, Confusion
	House of the Big Walker
	Books of Sand v1
	"Nemon's Memory Stone"
	various scrolls


Tamriel Rebuilt is a not-for-profit development team made up entirely of fans. We are not in any way, shape, or form associated or affiliated with Bethesda Softworks, Zenimax, or any other entity involved in Morrowind's original development. The Elder Scrolls is a registered trademark of Bethesda Softworks. All additional content produced outside Bethesda Softworks and the TES Construction Set remains the intellectual property of its creators.

All rights to original game assets belong to Bethesda Softworks. Additional assets created for Tamriel Rebuilt are the property of their creators first, and the organization second. The following resources should only be used in mods with a dependency on Tamriel_Data: Molecrab, Riverstrider, Telvanni Cephalopod. All other assets are free to use and redistribute in other modifications provided that their original author (where applicable) or Tamriel Rebuilt are duly credited.
The content of this archive is © Tamriel Rebuilt Community 2001-2023.
http://www.tamriel-rebuilt.org


Rev. 23.10
Updated 31 Oct. 2023
