# Contributing

*This is the contributing guide for contributing to the [OpenMW Package Repository](https://gitlab.com/portmod/openmw-mods). For information about contributing to portmod itself, see [the portmod docs](https://portmod.readthedocs.io/en/stable/contributing.html). For information about contributing to the wiki, see [the wiki homepage](https://gitlab.com/portmod/portmod/-/wikis/home).*

Common contributions can include:
- Finding and adding new mods to the repository (and ideally testing them if their stability is unknown).
- Updating mod packages which are present but out of date.
- Adding missing metadata such as conflicts and sorting rules.

See [Mod Maintainers](https://gitlab.com/portmod/portmod/-/wikis/Mod-Maintainers) if you want to help keep mod packages up to date. This is not mandatory: one-time updates to packages are always welcomed!

## Pre-requisites and Setup

It is generally recommended that you use a text editor or python IDE and a command line to create, edit and test packages. GitLab has two different online editors, however at the moment they lack any way to test packages in any way (and mods should ideally be tested in-game after being installed, something the online editors will never support).

To be able to submit changes to the package repository, you need to fork [the repository](https://gitlab.com/portmod/openmw-mods), create a feature branch, commit your changes to the feature branch and create a merge request to merge your changes.

If you are unfamiliar with this process, you may want to check out the [GitLab Basics Guide](https://docs.gitlab.com/ee/topics/git/)

In Particular, you may want to look at:

[Making your first commit](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html), which guides you through the basics of git, gitlab, and commits.
    - [Forking Workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html), which describes how to use forks to contribute to projects you don't have write access to.

### Getting Portmod to see  your local clone

You will need to adjust `repos.cfg` to reference your fork instead of the original package repository. This will allow you to install packages you have added or modified via the merge prefix subcommand, which is necessary for testing the package before you submit it.

Example repos.cfg
```cfg
[openmw]
location = /path/to/your/cloned/fork
# You may want to disable auto_sync, as it may not work properly on your fork
auto_sync = False
...
```

When you have finished working with the repository, you may want to revert your changes to `repos.cfg` to make sure the repository is kept up to date when you run portmod sync. Otherwise you will need to manually make sure your fork is up to date.

Also note that this change will mean that your profile symlink will still point to the profile in the original repository, unless you set up your fork at the same path as your original repository by replacing the directory or [adding your fork as a separate remote](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-a-remote). You may want to update your profile symlink, particularly if you want to make changes to the profiles.

### Licensing and Contribution Origins
Contributing to this repository ([openmw-mods](https://gitlab.com/portmod/openmw-mods)) means that you are licensing your contributions under the GPL version 3 or later.

When making contributions, you can optionally agree to the Gentoo Certificate of Origin by adding
```
Signed-off-by: Name <e-mail>
```
to your commit messages (as a separate line). This can be done automatically by committing using the flag `-s`/`--signoff` with `git commit`.

The following is revision 1 of the Gentoo Certificate of Origin:

> By making a contribution to this project, I certify that:
>
>    The contribution was created in whole or in part by me, and I have the right to submit it under the free software license indicated in the file; or
>
>    The contribution is based upon previous work that, to the best of my knowledge, is covered under an appropriate free software license, and I have the right under that license to submit that work with modifications, whether created in whole or in part by me, under the same free software license (unless I am permitted to submit under a different license), as indicated in the file; or
>
>    The contribution is a license text (or a file of similar nature), and verbatim distribution is allowed; or
>
>    The contribution was provided directly to me by some other person who certified 1., 2., 3., or 4., and I have not modified it.
>
>    I understand and agree that this project and the contribution are public and that a record of the contribution (including all personal information I submit with it, including my sign-off) is maintained indefinitely and may be redistributed consistent with this project or the free software license(s) involved.

The Gentoo certificate of Origin is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License, and was originally found [here](https://www.gentoo.org/glep/glep-0076.html#certificate-of-origin) (note that Portmod does not fall within the scope of the Gentoo project; it is just that it is useful for Portmod to make use of some of the Gentoo documentation and resources).

### Copyright Statements
Files must begin with a copyright statement such as included in `header.txt`.

In general, we use the convention that copyright is listed as
```
Copyright YEARS Portmod Authors
```
to refer to all contributors generally. All copyright is retained by the original authors, and specific authorship information can be obtained using git metadata.

Note that only files which have changed since the date given in the copyright statement should have their statement updated to include the current year.

## Required Tools

There are a few tools which are used by CI checks, and as such will be necessary for you to make use of if you want your merge request to be accepted.

### Inquisitor

Inquisitor (included with portmod) is used both to validate pybuild files (`inquisitor scan <pybuild>`), and to (re)generate `Manifest` files (`inquisitor manifest <pybuild>`).

### Black

[Black](https://github.com/psf/black) is used to format pybuild files, and ensures both that the files in this repository are formatted consistently, and that updates to files are concise and never include reformatting noise (since they'll be formatted correctly to start with).

### Flake8

[Flake8](https://gitlab.com/pycqa/flake8) enforces python-related QA checks, and should be used ahead of submissions to validate the python code within the pybuild files. Unlike black, flake8 will not fix the problems for you.

## Pre-commit

The checks run by CI can be run locally in an automatic fashion using [pre-commit](https://pre-commit.com/).
This repository has both pre-commit hooks for the tests, and a prepare-commit-msg hook, which will automatically generate commit messages for common operations such as adding new packages, or updating packages.

To get started with pre-commit (once you've installed it), run `pre-commit install -t pre-commit -t prepare-commit-msg` from within your local copy of the repository to install the hooks. Pre-commit will then automatically set up and run the checks when you next create a commit.

## Creating new Packages

There's a [Package Guide](https://gitlab.com/portmod/portmod/-/wikis/OpenMW/Package-Guide) which describes the basics of creating pybuild files, and a bunch of other resources on the [wiki](https://gitlab.com/portmod/portmod/-/wikis/OpenMW/OpenMW) and in the [Package Development](https://portmod.readthedocs.io/en/stable/dev/packages.html#package-development) section of the developer guide which explain the details of the system.

There's also a tool called [importmod](https://gitlab.com/portmod/importmod) which can be used to generate partial build files for mods automatically. Note that this tool is somewhat undocumented, and may not be particularly stable.

## Updating revisions when fixing packages

When updating or fixing an existing version of a package, you may need to bump the package revision. This means renaming the file to add a revision number, or increase the revision number by 1.

E.g.
``package-name-1.0.pybuild`` when bumped becomes ``package-name-1.0-r1.pybuild``

See :ref:`version-syntax` for details on revisions.

You should bump the revision if, and only if, it is necessary for users who already have the package installed to install the new version.

* If a package is being modified because it always fails to build, the revision does not need to be changed since no-one can install it.
* If a package is being modified because it sometimes fails to build, the revision does not need to be changed as long as there are no issues for those for whom it installed correctly.
* If a package is being modified to change some piece of non-functional documentation such as the name, description or homepage, the revision does not need to be changed.
* If a package is being modified because it installs without error, but there is an issue with the installed data, the revision should be changed.

## Package Updates
Ideally new packages could be introduced with testing keywords and stabilized later to reduce the impact of any problems, but that process hasn't really been set up since it would add more work and we haven't had a lot of contributors.

However for the most reliable experience at the moment, you should leave at least the previous version of a package to give people something to fall back to if there are problems with the update.

Old versions can be removed if their maintenance becomes a burden, and to reduce the number of old versions to maintain, old patch versions can be removed first, and eventually old minor versions, but it may be helpful to keep packages for each major version and each recent minor version. This assumes that the package follows semantic versioning, but should generally work reasonably well even when they don't.

E.g. instead of keeping versions `1.0.0`, `1.0.1`, `1.0.2`, `1.1.0`, `1.1.1`, `2.0.0`, `2.0.1`, `2.1.0`, etc. Just keep `1.1.1` (latest from major version 1), `2.0.1` (latest from minor version `2.0`) and `2.1.0` (latest overall).
Then, if `2.1.1` gets added, it would be a good idea to also keep `2.1.0` in case there are issues with `2.1.1`, but it can eventually be removed when `2.1.1` has proven itself to be stable.
