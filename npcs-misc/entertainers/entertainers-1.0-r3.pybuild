# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Entertainers"
    DESC = "Adds the option to entertain the patrons of the Eight Plates in Balmora"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    DEPEND = ">=bin/delta-plugin-0.19"
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/entertainers.zip
        https://gitlab.com/bmwinger/umopp/uploads/a6486d327b1f5caf45a5e2e491b66b65/entertainers-umopp-3.2.0.2.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("entertainers.esp")], S="entertainers")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "entertainers-umopp-3.2.0.2")
        self.execute(
            "delta_plugin -v apply " + os.path.join(path, "entertainers-patch.yaml")
        )
