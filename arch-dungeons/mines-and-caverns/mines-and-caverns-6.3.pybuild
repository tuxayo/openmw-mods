# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.fix_maps import FixMaps
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, FixMaps, MW):
    NAME = "Mines and Caverns"
    DESC = "This mod improves and/or expands several vanilla dungeons"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/44893
        darker? ( https://www.nexusmods.com/morrowind/mods/47127 )
    """
    KEYWORDS = "openmw"
    # Permissions section on NexusMods says that you can redistribute,
    # however this mod bundles a lot of content from other mods, and while
    # attribution is included, it's not clear if permission was obtained,
    # and some included mods do not have a license that provides derivation
    # rights (or even licenses at all in some cases).
    LICENSE = "all-rights-reserved"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/44893"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
    """
    IUSE = "devtools darker"
    # Not compatible with bloated caves
    # Readme states that it's not compatible with gameplay-misc/true-lights-and-darkness
    # however this is apparently just due to it not affecting the new cells, so it is
    # not a significant enough conflict to require a blocker
    MAIN_FILE = "Mines_and_Caverns-44893-6-3-1681144662"
    TEST_CELL = "MC_test_cell-44893-6-3-1682874994"
    SRC_URI = f"""
        {MAIN_FILE}.rar
        devtools? ( {TEST_CELL}.rar )
        darker? ( https://gitlab.com/portmod-mirrors/openmw/-/raw/master/Darker_Morrowind_Mods-47127-1-0-2-1584666728.7z )
    """
    # Mines_and_Caverns-DD-44893-5-5-1575336607.rar
    # MC_Animated_Morrowind_II-44893-1-0-1609344265.rar
    # Julan_Ashlander_MC_patch-44893-1-0-1564857480.rar
    NORMAL_MAP_PATTERN = "_NM"
    INSTALL_DIRS = [
        InstallDir(
            "Mines & Caverns",
            PLUGINS=[File("Clean_Mines & Caverns.esp")],
            S=MAIN_FILE,
        ),
        # Patch for Julan Ashlander Companion
        # InstallDir(
        #     "KS_Julan_Ashlander - MC_patch",
        #     PLUGINS=[File("Clean_KS_Julan_Ashlander - MC_patch.esp")],
        #     S="Julan_Ashlander_MC_patch-44893-1-0-1564857480",
        # ),
        # Patch for Detailed Dungeons
        # InstallDir(
        #     "Detailed Dungeons",
        #     PLUGINS=[File("Clean_Detailed Dungeons.esp")],
        #     S="Mines_and_Caverns-DD-44893-5-5-1575336607",
        # ),
        # Patch for animated morrowind II
        # FIXME: This patch is for the original, so it would need to be merged
        # With the openmw version we use, perhaps by turning it into an actual.
        # patch rather than having it override the esp.
        # InstallDir(
        #    "MC_Animated Morrowind II/Data Files",
        #    PLUGINS=[File("MC_Animated Morrowind II.esp")],
        #    S="MC_Animated_Morrowind_II-44893-1-0-1609344265",
        # ),
        InstallDir(
            ".",
            S="Darker_Morrowind_Mods-47127-1-0-2-1584666728",
            PLUGINS=[File("Darker Mines and Caverns.esp")],
            REQUIRED_USE="darker",
            WHITELIST=["Darker Mines and Caverns.esp"],
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Clean_MC_test cell.esp")],
            S=TEST_CELL,
            REQUIRED_USE="devtools",
        ),
    ]
